package com.example.fulecostcalculation;

import android.widget.Toast;

public class Algoritm {
    private float gravityEarch = 9.80665f;
    private float gravityMars = 3.721f;
    public MainActivity mainActivity;


    public float fuleCalculate(float mass){

        float fuleMass = mass * 100 * gravityMars / gravityEarch;
        //Toast.makeText(mainActivity, String.valueOf(fuleMass), Toast.LENGTH_SHORT).show();

        return fuleMass;
    }
}
